#!/usr/bin/perl

#
# Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.
#



# This script creates tables for *.db2 in MySQL database, parse them and inserts values inside tables.
# CatArmory needs these tables - otherwise it won't work.


use DBI;

my $database = 'wowarmory';             # Do not use database like trinity_world or trinity_characters (performance/security). I really recommend using one separate database for DBCs and DB2s
my $interface = '192.168.100.3';        # MySQL server host or ip
my $user = 'trinity';                   # Username to database
my $password = 'trinity';               # Password to database (need create, drop, insert, select privileges)
my $db2_fmt_dir = '.';                  # Current directory (use provided DB2fmt.h - it is slighty modified version of the original from trinity source)
my $db2_dir = '/usr/cataclysm/dbc';     # Your DB2 directory (where *.db2 files are)


#############################################
#                                           #
#  No need to edit anything below this line #
#                                           #
#############################################

$| = 1;

my $dbh = init();

   open (FILE, $db2_fmt_dir.'/DB2fmt.h');
   my $struct;
   while (my $line = <FILE>) {
      chomp ($line);
      next if ($line =~ /\/\//);
      if ($line =~ m/char const (.+)Entryfmt\[\] *= *"(.+?)";/ || $line =~ m/char const (.+)fmt\[\] *= *"(.+?)";/) {
         my $file = $1;
         my $chars;
         @{$chars} = split(//,$2);
         $struct->{lc ($file).'.db2'} = $chars;
      }
   }
   close (FILE);


   my $created;

      if (-d $db2_dir) {

         opendir(DIR, $db2_dir) or warn "can't opendir $db2_dir: $!";
         foreach my $file (grep { !/^\./ && -f $db2_dir.'/'.$_ && $_ =~ /\.db2$/ } readdir(DIR)) {

            if (defined($created->{$file}) && $created->{$file} == 1) {next;}
#next if ($file ne 'SpellIcon.db2');

            $file =~ /^(.+?)\.db2$/;
            my $file_base = $1;
            $file_base =~ s/\-/_/gs;
            
#            next if ($file_base ne "KeyChain");

            print 'Creating table for: '.$file_base." -> db2_".lc($file_base)."\n";

            if (!$struct->{lc $file}) {
               print "no structure definition found for ".$file." ... skipping\n";
               next;
            }

            open (FILE, $db2_dir.'/'.$file);
            my ($header) = &proc_header(*FILE,1);
            close (FILE);

            $dbh->do("DROP TABLE IF EXISTS db2_".lc($file_base));

            my $create_query = "CREATE TABLE db2_".lc($file_base)." (";
            $create_query .= "id int(10) unsigned NOT NULL auto_increment,";
            my $i = 0;
            my $indexes = [ 'KEY `col_0` (`col_0`)' ];

            foreach (@{$struct->{lc $file}}) {
               if ($_ eq 'i') {
                  $create_query .= "col_".$i." int NULL,";
               } elsif ($_ eq 'b') {
                  $create_query .= "col_".$i." tinyint NULL,";
               } elsif ($_ eq 'l') {
                  $create_query .= "col_".$i." boolean NULL,";
               } elsif ($_ eq 's') {
                  $create_query .= "col_".$i." varchar(255) NULL,";
               } elsif ($_ eq 'f') {
                  $create_query .= "col_".$i." real(12,6) NULL,";
               } elsif ($_ eq 'n' || $_ eq 'd') {
                  $create_query .= "col_".$i." int NULL,";
                  if ($i>1) {
                     push @{$indexes}, 'KEY `col_'.$i.'` (`col_'.$i.'`)';
                  }
               } else {
                  $create_query .= "col_".$i." int(10) NULL,";
               }
               ++$i;
            }
            $create_query .= 'PRIMARY KEY (id), '.(join(', ', @{$indexes})).') ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';

            $created->{$file} = 1;

            $dbh->do($create_query);
            print "\n";
         }
      }
      

      if (-d $db2_dir) {
         opendir(DIR, $db2_dir) or die "can't opendir $db2_dir: $!";
         foreach my $file (grep { !/^\./ && -f $db2_dir.'/'.$_ && $_ =~ /\.db2$/} readdir(DIR)) {

            $file =~ /^(.+?)\.db2$/;
            my $file_base = $1;
            $file_base =~ s/\-/_/gs;
            
#            next if ($file_base ne "KeyChain");

           
            print 'Progress: 0% --------------------------------------------------------------------------------------------------- 100%'."\n";
            print 'Reading data from '.$file_base.":\n";
            if (!$struct->{lc $file}) {
               print " no structure definition found ... skipping\n";
               next;
            }
            open (FILE, $db2_dir.'/'.$file);
            my ($header) = &proc_header(*FILE);
            my $db2_hashref = &proc_data(*FILE,$header,$struct->{lc $file});
            close (FILE);
            print "\n";
            print 'Inserting data into table db2_'.lc($file_base).': '."\n";
            print '             ';
   
            my $select_query = 'SELECT id';
            my $insert_query = 'INSERT INTO db2_'.lc($file_base).' (id';
            for (my $i=0; $i<scalar(@{$struct->{lc $file}}); ++$i) {
               $insert_query .= ',col_'.$i;
               $select_query .= ',col_'.$i;
            }
            $insert_query .= ') VALUES (0'.",?"x(scalar(@{$struct->{lc $file}}));
            $insert_query .= ')';

            $select_query .= ' FROM db2_'.lc($file_base).' WHERE col_0=?';

            my $insert = $dbh->prepare($insert_query);

            my $prg = 0;
            my $j = 0;
            foreach my $row (@{$db2_hashref}) {
               my $progress = int($j / $header->[0] * 100);
               if ($progress > $prg) {
                  print '#'x($progress-$prg);
               }
               $prg = $progress;
               ++$j;

               my $i = 0;
               foreach my $col (@{$row}) {
                  if ($struct->{lc $file}->[$i] eq 'f') {
                     $col = unpack("f", pack("V", $col));
                  } elsif ($struct->{lc $file}->[$i] eq 'i' || $struct->{lc $file}->[$i] eq 'n') {
                     $col = unpack("i", pack("V", $col));
                  }
                  ++$i;
               }

               if (scalar(@{$struct->{lc $file}}) == scalar(@{$row})) {
                  my $select = $dbh->prepare($select_query);
                  $select->execute($row->[0]);
                  if ($select->rows() == 0) {
                     $insert->execute(@{$row});
                  } else {
                     my $selected_data = $select->fetchrow_hashref();
                     my $i = 0;
                     foreach my $col (@{$row}) {
                        if ((!defined($selected_data->{'col_'.$i}) || $selected_data->{'col_'.$i} eq '') && defined($col) && $col ne '') {
                           my $update = $dbh->prepare('UPDATE db2_'.lc($file_base).' SET col_'.$i.'=? WHERE col_0=?');
                           $update->execute($col,$row->[0]);
                        }
                        ++$i;
                     }
                  }
               } else {
                 print "\n";
                 warn $file. "fatal error... fmt counts: " . scalar(@{$struct->{lc $file}}) ." columns. Detected columns: ". scalar(@{$row});
                 die;
               }
            }
            if ($prg < 99) {
               print '#'x(99-$prg);
            }

            print "\n".'             Succesfully processed all '.scalar(@{$db2_hashref}). " rows\n\n";

         }

      }


sub cvt($) {
   my $in = shift;
   my $hex = substr($in, 3, 1).substr($in, 2, 1).substr($in, 1, 1).substr($in, 0, 1);
   return hex(unpack("H8", $hex));
}


sub proc_header($$) {
   my ($f,$v) = @_;
   read $f,my $buffer,4;
   if ($buffer ne 'WDB2') {
      warn "no DB2";
   }
   my $output;
   for (my $i=0;$i<11;++$i) {
      read $f,$buffer,4;
      push @{$output}, cvt($buffer);
   }
   
   if ($v) {
      print "\tRows: ".$output->[0]."\n";
      print "\tColumns: ".$output->[1]."\n";
      print "\tBytes per row: ".$output->[2]."\n";
      print "\tString size: ".$output->[3]."\n";
      print "\tTable hash: ".$output->[4]."\n";
      print "\tClient minor: ".$output->[5]."\n";
      print "\tUnk: ".$output->[6]."\n";
      print "\tFirst row index: ".$output->[7]."\n";
      print "\tLast row index: ".$output->[8]."\n";
      print "\tLocale: ".$output->[9]."\n";
      print "\tUnk: ".$output->[10]."\n";
   }
   
   if ($output->[8] != 0) {
      read $f,$buffer, (($output->[8] * 4) - 48);
      read $f,$buffer, (($output->[8] * 2) - (48));
   }
   
   return $output;
}

sub proc_data($$) {
   my ($f,$header,$struct) = @_;
   my $prg = 0;
   my $rows;

   print '             ';
   for (my $i=0;$i<$header->[0];++$i) {
      my $progress = int($i / $header->[0] * 33.34);
      if ($progress > $prg) {
         print '#'x($progress-$prg);
      }
      $prg = $progress;
      my $cols;
      for (my $j=0;$j<$header->[1];++$j) {
         read $f,my $buffer,4;
         push @{$cols}, cvt($buffer);
        
      }
      push @{$rows}, $cols;
   }
   if ($prg < 33) {
      print '#'x(33-$prg);
   }
            

   my $data;
   my $storage = 0;
   my $prg = 0;
   for (my $i=0;$i<$header->[3];++$i) {
      my $progress = int($i / $header->[3] * 33.34);
      if ($progress > $prg) {
         print '#';
      }
      $prg = $progress;
      read $f,my $buffer,1;
      if ($buffer eq chr(0)) {
         $storage=$i+1;
         next;
      }
      if ($storage > 0) {
         $data->{$storage} .= $buffer;
      }
   }
   if ($prg < 33) {
      print '#'x(33-$prg);
   }   
   
   my $prg = 0;
   my $j = 0;
   foreach my $row (@{$rows}) {
      my $i = 0;
      my $progress = int($j / $header->[0] * 33.34);
      if ($progress > $prg) {
         print '#'x($progress-$prg);
      }
      $prg = $progress;
      foreach my $col (@{$row}) {
         if ($struct->[$i] eq 's') {
            $col = $data->{$col};
         }
         ++$i;
      }
      ++$j;
   }
   if ($prg < 33) {
      print '#'x(33-$prg);
   }
   return $rows;
}

sub init() {
   my $dbh;
   if ($dbh = DBI->connect("DBI:mysql:database=".$database.";host=".$interface, $user, $password)) {
      $dbh->do('SET NAMES "utf8"');
      return $dbh;
   } else {
      return 0;
   }
}

