#!/usr/bin/perl

#
# Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.
#

   opendir(DIR, '.') || die "can't opendir: $!";
   my @pngs = grep { !/^\./ && -f "./$_" && $_ =~ /\.png$/i} readdir(DIR);
   closedir DIR;


   foreach (@pngs) {
      `/usr/bin/pngtopnm "$_" > /tmp/temp.pnm`;
      `/usr/bin/pnmscale -xysize 32 32 /tmp/temp.pnm > resized_temp.pnm`;
      `/usr/bin/pnmtopng resized_temp.pnm > "../icons_32/$_"`;
      unlink '/tmp/temp.pnm';
      unlink 'resized_temp.pnm';
   }

