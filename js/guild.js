/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Guild class
 * @class Guild
 * @singleton
 */
Guild = (function(g) {
	var Guild = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Guild.prototype, {
		/**
		 * Initialize Guild
		 * @param {Object} c Guild object data from server
		 */
		init: function(g) {
			this.guildId = g.guildid;
			this.name = g.guildName;
			this.faction = (1<<(g.leaderRace-1) & racemaskAlliance ? 1 : 0);
			this.level = g.guildLevel;
			this.xp = g.experience;
			this.todayXp = g.todayExperience;
			this.leader = g.leaderName;
			// call methods
			this.initBasic();
		},

		/**
		 * Init basic stuff - render
		 */
		initBasic: function() {
			$('#flag').css('background','url(\'/images/b_'+this.faction+'.jpg\')');
			$('#guild_name').html(this.name);
			$('#guild_level').text(this.level);
			
		},

		/**
		 * Return Guild guid
		 * @return guid
		 */
		getGuildId: function() {
			return this.guildId;
		},
	});

	return Guild;
})();

