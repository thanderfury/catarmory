/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Quest class
 * @class Quest
 * @singleton
 */
Quest = (function(i) {
	var Quest = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Quest.prototype, {
		/**
		 * Initialize quest
		 * @param {Object} q quest object data from server
		 */
		init: function(q) {
			this.id = q.Id;
			this.title = q.Title;
			this.objectives = q.Objectives;
            this.details = q.Details;
            this.method = q.Method;
            this.level = q.Level
            this.minLevel = q.MinLevel;
            this.requiredRaces = q.RequiredRaces;
            this.zone = q.ZoneOrSort;
            this.type = q.Type;
            this.money = q.RewardOrRequiredMoney;
            this.xp = q.Experiences;
            this.requiredNpcs = q.RequiredNpcs;
            this.requiredObjects = q.RequiredObjects;
            this.requiredItems = q.RequiredItems;
            this.rewardItems = q.RewardItems;
            this.chooseItems = q.ChooseItems;
            this.reputations = q.GainReputations;
            this.creatureStarters = q.CreatureStarters;
            this.creatureEnders = q.CreatureEnders;
            this.preQuests = q.Prequests;
            this.nextQuests = q.Nextquests;
			// call methods
			this.initBasic();
		},

		/**
		* Init basic stuff - render
		*/
		initBasic: function() {
			// reset containers
			$('#quest_line').html('');
			$('#required').html('');
			$('#rewards').html('');
			$('#gains').html('');
			$('#quest_info').html('');
			
			
			$('#quest_title').html(this.title);
			$('#quest_level').html(this.level);
			$('#quest_objectives').html(this._formatText(this.objectives));
			$('#quest_details').html(this._formatText(this.details));

			// info window on right side
			var $ul_info = $('<ul class="info-window"></ul>');
			$ul_info.append('<li><div>Level: '+this.level+'</div></li>');
			$ul_info.append('<li><div>Start: '+this._showCreatures(this.creatureStarters)+'</div></li>');
			$ul_info.append('<li><div>End: '+this._showCreatures(this.creatureEnders)+'</div></li>');
			
			$ul_info.append('<li><div>Min. Level: '+this.minLevel+'</div></li>');
			$ul_info.append('<li><div>Races: '+this.requiredRaces+'</div></li>');
		
			$('#quest_info').append($ul_info);

			if (this.preQuests.groups.length > 0) {
				var $div = $('<div>Prequests:</div>');
				$('#quest_line').append($div);
				var $groups = $('<ul></ul>');
				$('#quest_line').append($groups);
				for (var i in this.preQuests.groups) {
					if (this.preQuests.groups[i] == 'simple') {
						var $group = $('<li>This quest needs to be done:</li>');
						$groups.append($group);
						$group.append(this._showQuests(this.preQuests.data['simple']));
					} else if (this.preQuests.groups[i] > 0) {
						var $group = $('<li>Only one quest from this group can be done:</li>');
						$groups.append($group);
						$group.append(this._showQuests(this.preQuests.data[this.preQuests.groups[i]]));
					} else if (this.preQuests.groups[i] < 0) {
						var $group = $('<li>All quests from this group needs to be done:</li>');
						$groups.append($group);
						$group.append(this._showQuests(this.preQuests.data[this.preQuests.groups[i]]));
					} 
				}
			}
			

			if (this.nextQuests.groups.length > 0) {
				var $div = $('<div>Next quests:</div>');
				$('#quest_line').append($div);
				var $groups = $('<ul></ul>');
				$('#quest_line').append($groups);
				for (var i in this.nextQuests.groups) {
					if (this.nextQuests.groups[i] == 'simple') {
						var $group = $('<li>This quest leads to:</li>');
						$groups.append($group);
						$group.append(this._showQuests(this.nextQuests.data['simple']));
					} else if (this.nextQuests.groups[i] > 0) {
						var $group = $('<li>After completing, one quest from this group can be picked:</li>');
						$groups.append($group);
						$group.append(this._showQuests(this.nextQuests.data[this.nextQuests.groups[i]]));
					} else if (this.nextQuests.groups[i] < 0) {
						var $group = $('<li>After completing, all quests from this group can be picked:</li>');
						$groups.append($group);
						$group.append(this._showQuests(this.nextQuests.data[this.nextQuests.groups[i]]));
					} 
				}
			}
			
			

			
			
			// required items
			var $wrap = $('<div></div>');
			if (this.requiredItems.length > 0) {
				var $req = $('<div></div>');
				for (var i in this.requiredItems) {
					var display = new DisplayItem({
						entry: this.requiredItems[i].itemEntry,
						icon: this.requiredItems[i].icon,
						count: this.requiredItems[i].count,
						name: this.requiredItems[i].name,
						quality: this.requiredItems[i].quality
					})
					var $div = $('<div></div>');
					$div.append(display.getIconWithName({
						size: 32,
						hideLinkCount: true,
					}));	// get jquery object and append it
					$req.append($div);
				}
				$wrap.append($req);
			}
			$('#required').append($wrap);

			if (this.money < 0) {
				var $txt = ('<div>Have to pay:</div>');
				$('#required').append($txt);
				var $money = $('<div></div>');
				var money = new Money(this.money);
				$money.append(money.getHTMLMoney());
				$('#required').append($money);
			}

			// rewards (items / gold)
			var $wrap = $('<div></div>');
			if (this.chooseItems.length > 0) {
				var $table = $('<table style="border-spacing: 0;"></table>');
				var $tr = $('<tr></tr>');
				$table.append($tr);
				
				var $txt = ('<div>Able to choose one of:</div>');
				$wrap.append($txt);
				for (var i in this.chooseItems) {
					var display = new DisplayItem({
						entry: this.chooseItems[i].itemEntry,
						icon: this.chooseItems[i].icon,
						count: this.chooseItems[i].count,
						name: this.chooseItems[i].name,
						quality: this.chooseItems[i].quality
					})
					var $td = $('<td></td>');
					$td.append(display.getIconWithName({
						size: 32,
						hideLinkCount: true,
					}));	// get jquery object and append it
					$tr.append($td);
				}
				$wrap.append($table);
			}

			if (this.chooseItems.length > 0 || this.money > 0 || this.rewardItems.length > 0) {
				var $txt = ('<div>Also receive:</div>');
				$wrap.append($txt);
			} else {
				var $txt = ('<div>Receive:</div>');
				$wrap.append($txt);
			}
			
			if (this.money > 0) {
				var $money = $('<div></div>');
				var money = new Money(this.money);
				$money.append(money.getHTMLMoney());
				$wrap.append($money);
			}

			if (this.rewardItems.length > 0) {
				var $table = $('<table style="border-spacing: 0;"></table>');
				var $tr = $('<tr></tr>');
				$table.append($tr);
				
				var $txt = ('<div></div>');
				$wrap.append($txt);
				for (var i in this.rewardItems) {
					var display = new DisplayItem({
						entry: this.rewardItems[i].itemEntry,
						icon: this.rewardItems[i].icon,
						count: this.rewardItems[i].count,
						name: this.rewardItems[i].name,
						quality: this.rewardItems[i].quality
					})
					var $td = $('<td></td>');
					$td.append(display.getIconWithName({
						size: 32,
						hideLinkCount: true,
					}));	// get jquery object and append it
					$tr.append($td);
				}
				$wrap.append($table);				
			}
			$('#rewards').append($wrap);
			
			
			if (this.reputations.length > 0 || this.xp) {
				var $ul = $('<ul style="padding: 0 20px"></ul>');
				
				$ul.append('<li>'+this.xp+' experiences</li>');
				
				for (var i in this.reputations) {
					$ul.append('<li>'+this.reputations[i].value+' reputation with '+this.reputations[i].name+'</li>');
				}
				$('#gains').append($ul);
			}
			
			
		},
		
		_formatText: function(text) {
			text = text.replace(/\$b/gi,'<br>').replace(/\$n/gi,'&lt;Name&gt;');
			return text;
		},
		
		_showCreatures: function(arr) {
			var out = '';
			for (var i in arr) {
				out = '<a href="npc.html#'+arr[i].entry+'">'+arr[i].name+'</a><br />';
			}
			return out;
		},
		
		_showQuests: function(obj) {
			var $ul = $('<ul></ul>');
			for (var j in obj) {
				console.log('obj[j].RequiredRaces = '+obj[j].RequiredRaces+' & racemaskAlliance = '+racemaskAlliance);
				$ul.append('<li>'+(obj[j].RequiredRaces ? '<img src="/images/' + (obj[j].RequiredRaces & racemaskAlliance ? 'alliance' : 'horde') + '.png"> ' : '' )+'<a href="quest.html#'+obj[j].Id+'"><span style="color:#FFD100">'+obj[j].Title+'</span></a</li>'); 
			}
			return $ul;
		}
		
	});

	return Quest;
})();
  