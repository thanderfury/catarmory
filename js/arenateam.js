/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
 /**
 * Arenateam class
 * @class Arenateam
 * @singleton
 */
 
Arenateam = (function(a) {
	var Arenateam = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Arenateam.prototype, {
		/**
		 * Initialize Arenateam
		 * @param {Object} a Arenateam object data from server
		 */
		init: function(a) {
			this.teamId = a.arenaTeamId;
			this.name = a.arenateamName;
			this.faction = (1<<(a.captainRace-1) & racemaskAlliance ? 1 : 0);
			// call methods
			this.initBasic();
		},

		/**
		 * Init basic stuff - render
		 */
		initBasic: function() {
			$('#flag').css('background','url(\'/images/b_'+this.faction+'.jpg\')');
			$('#arenateam_name').html(this.name);
		
		},

		/**
		 * Return Arenateam id
		 * @return {Number} id
		 */
		getTeamId: function() {
			return this.teamId;
		},
	});

	return Arenateam;
})();

