/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Item class
 * @class Item
 * @singleton
 */
Item = (function(i) {
	var Item = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Item.prototype, {
		/**
		 * Initialize item
		 * @param {Object} i item object data from server
		 */
		init: function(i) {
			this.guid = i.guid;
			this.entry = i.itemEntry;
			this.bag = i.bag;
			this.slot = i.slot;
			this.count = i.count;
			this.name = i.name;
			this.quality = i.quality;
			this.itemLevel = i.itemLevel;
			this.inventoryType = i.inventoryType;
			this.enchantments = i.enchantments;
			this.icon = i.icon;
			this.ContainerSlots = i.ContainerSlots;

			// jQuery element of item
			this.$el = $(
'<div data-guid="'+this.guid+'" class="item" style="position: relative">\
<a href="item.html#'+this.entry+'">\
<img style="" src="icons/'+this.icon+'.png">\
<img style="position: absolute; left: 0; top: 0" src="images/glow_quality_'+this.quality+'.png"></a>'+
(this.count > 1 ? '<div style="position:absolute; top: 0; margin: 45px 0 0 50px; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black">'+this.count+'</div>' : '')+'\
</div>'
			);
			
			this.$link = $('<div data-guid="'+this.guid+'" class="item">\
<a href="item.html#'+this.entry+'"  style="color:'+ItemColors[this.quality]+'">['+this.name+']</a>\
</div>');
		}
	});

	return Item;
})();
  