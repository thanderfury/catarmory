/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Character class
 * @class Character
 * @singleton
 */
Character = (function(c) {
	var Character = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Character.prototype, {
		/**
		 * Initialize character
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.guid = c.guid;
			this.name = c.name;
			this.race = c.race;
			this.klass = c['class'];
			this.level = c.level;
			this.p_faction = (1<<(c.race-1) & racemaskAlliance ? 1 : 0);
			this.guildId = c.guildid;
			this.guildName = c.guildName;
			this.guildRank = c.guildRank;
			
			// call methods
			this.inventory = new Inventory(this);
			this.skills = new Skills(this);
			this.talents = new Talents(this);
			this.quests = new Quests(this);
			this.initBasic();
			this.initQuests();
		},

		/**
		 * Init basic stuff - render
		 */
		initBasic: function() {
		// header (name, banner color, guild, ...)
			$('#flag').css('background','url(\'/images/b_'+this.p_faction+'.jpg\')');
			//$("body").css("background", "url(\'http://static.wowarmory.sk/images/"+ ((this.p_faction == 0) ? 'deathwing' : 'lichking')  +"_background.jpg\') no-repeat scroll center top #000000");
			$('.char_inventory').css("background", 'url(\'/images/inventory_'+this.p_faction+'.jpg\')');
			$('#char_name2').html(this.name);
			$('#char_race').html('<img src="/images/race_'+this.race+'.png">');
			$('#char_class').html('<img src="/images/class_'+this.klass+'.png">');
			$('#char_level').html(this.level);
			if (this.guildId) {
				$('#char_guild_name').html('<a href="guild.html#'+this.guildName+'/basic">'+this.guildName+'</a>');
				$('#char_guild_rank').html('&lt;'+this.guildRank+'&gt;');
			} else {
				$('#char_guild_name').html(L12N('not_in_guild'));
			}
			
			
			// power bar
			$('#power_bar').removeClass();
			if (this.klass == 1) { //rage
				$('#power_name').html('Rage');
				$('#power_bar').addClass('rage_bar');
			} else if (this.klass == 2 || this.klass == 3 || this.klass == 5 || this.klass == 7 || this.klass == 8 || this.klass == 9 || this.klass == 11) { //mana
				$('#power_name').html('Mana');
				$('#power_bar').addClass('mana_bar');
			} else if (this.klass == 4) { //energy
				$('#power_name').html('Energy');
				$('#power_bar').addClass('energy_bar');
			} else if (this.klass == 6) { // runic
				$('#power_name').html('Runic');
				$('#power_bar').addClass('runic_bar');
			}
		},
		
		initQuests: function() {
			var scope = this; 
			var $ul = $('<ul></ul>');
			for (var i=0; i<=4; ++i) {
				$main_zone = $('<li><div style="font-weight: bold">'+L12N('main_zone_'+i)+'</div></li>');
				$ul.append($main_zone);
				
				var $sub_zone = $('<ul></ul>'); 
				$main_zone.append($sub_zone);
				
				for (var j in zones[i]) {
					$zone = $('<li data-value="'+zones[i][j]+'">'+L12N('zone_'+zones[i][j])+'</li>');
					$sub_zone.append($zone);
					$zone.click(function() {
						var zone = $(this).attr('data-value');
						scope.quests.retrieveZone(zone);
					})
				}
				
				
			}
			$ul.treeview();		// 3rd party treeview
			
			$('#quests_categories').append($ul)
		},

		/**
		 * Return character guid
		 * @return guid
		 */
		getGuid: function() {
			return this.guid;
		},
	});

	return Character;
})();

/**
 * Skills class
 * @class Skills
 * @singleton
 */
 
Skills = (function(c) {
	var Skills = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Skills.prototype, {
		/**
		 * Initialize character skills
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.skills = [];
			this.retrieveSkills();
		},

		/**
		 * Get skills from server and render them
		 */
		retrieveSkills: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'skills',
					guid: this.character.getGuid()
				}, function(response) {
					var s = Main.evaluate(response);
					for (var i in s) {
						if (typeof(this.skills[s[i]['grp']]) === "undefined") {
							this.skills[s[i]['grp']] = [];
						}
						this.skills[s[i]['grp']].push(s[i]);
					}
					this.renderSkills();
				},this
			);
		},
		
		/**
		 * Render skills
		 */
		renderSkills: function() {
			var out ='';
			for (var i=0;i<this.skills[11].length;++i) {
				out += '<div class="tradeskill_wrapper"><div class="tradeskill"><img style="float:left; padding-right: 5px;" src="/icons_14/'+this.skills[11][i].icon+'.png"><div style="color: black; float: left;">'+this.skills[11][i].name+'</div><div style="color: #666; float: right;">';
				out += this.skills[11][i].value+' '+' / '+this.skills[11][i].max;
				out += '</div><div class="clear"></div></div></div>';
			}
			$('#tradeskills_recap').html(out);
		}
	});
	
	return Skills;
})();




/**
 * Talents class
 * @class Talents
 * @singleton
 */
 
Talents = (function(c) {
	var Talents = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Talents.prototype, {
		/**
		 * Initialize character talents
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.tabs = {};
			this.talents = {primary: {}, secondary: {}};
			this.retrieveTalents();
		},

		/**
		 * Get talents from server and render them
		 */
		retrieveTalents: function() {
			Main.fetch(
				{
					what: 'char',
					action: 'talents',
					guid: this.character.getGuid()
				}, function(response) {
					var s = Main.evaluate(response);

					for (var i in s['tabs']) {
						this.tabs[s['tabs'][i].tree_id] = s['tabs'][i];
					
						this.talents.primary[s['tabs'][i].tree_id] = [];
						this.talents.secondary[s['tabs'][i].tree_id] = [];
					}
					
					//alert(this.talents[851])
					
					for (var i in s['talents']) {
						if (s['talents'][i].spec == 0) {
							this.talents.primary[s['talents'][i].grp].push(s['talents'][i]);
						} else {
							this.talents.secondary[s['talents'][i].grp].push(s['talents'][i]);
						}
					}

					this.renderTalents();
				},this
			);
		},

		/**
		 * Get talent tree specialization depending on talents distribution
		 * @param {String} build "primary" or "secondary"
		 * @return {Object} talent tree object
		 */
		getCharacterSpec: function(build) {	// primary or secondary
			var topCount = 0;
			var tree = null;
			
			for (var i in this.talents[build]) {
				if (this.talents[build][i].length > topCount) {
					topCount = this.talents[build][i].length;
					tree = this.tabs[i];
				}
			}
			return tree;
		},

		/**
		 * Render talents
		 */
		renderTalents: function() {
			var points = []
			for (var i in this.tabs) {
				points.push(this.talents['primary'][i].length);
			}
			$('#primary_name').html(talentTabs[this.getCharacterSpec('primary').tree_id]);
			$('#primary_icon').html('<img src="/icons_32/'+(this.getCharacterSpec('primary').icon)+'.png">');
			$('#primary_points').html(points.join(' / '));

			if (secondary = this.getCharacterSpec('secondary')) {
				var points = []
				for (var i in this.tabs) {
					points.push(this.talents['secondary'][i].length);
				}
				$('#secondary_name').html(talentTabs[this.getCharacterSpec('secondary').tree_id]);
				$('#secondary_icon').html('<img src="/icons_32/'+(this.getCharacterSpec('secondary').icon)+'.png">');
				$('#secondary_points').html(points.join(' / '));
			} else {
				$('#secondary_name').html('No spec');
	            $('#secondary_icon').html('<img src="/icons_32/inv_misc_questionmark.png">');
	            $('#secondary_points').html('- / - / -');

			}
		},
	});
	
	return Talents;
})();






/**
* Quests class
* @class Quests
* @singleton
*/

Quests = (function(c) {
	var Quests = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Quests.prototype, {
		/**
		 * Initialize character Quests
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.quests = [];
		},

		/**
		 * Get Quests from server and render them
		 */
		retrieveZone: function(zone) {
			
			
			Main.fetch(
				{
					what: 'char',
					action: 'quests',
					guid: this.character.getGuid(),
					zone: zone
				}, function(response) {
					var completedQuests = Main.evaluate(response);

					Main.fetch(
						{
							what: 'quests',
							action: 'by_zone',
							zone: zone
						}, function(response) {
							var quests = Main.evaluate(response);
							
							$('#quests_pane').html('');
							var list = new List(ListView.templates.quests,quests,{
								hide: ['ZoneOrSort'],
								add: [
								    {
								    	id: 'Completed',
								    	name: 'Completed',
										align: 'center',
										text: function(t,d,s) {
											return search_in_array(d.Id,s.additionalData.quests) ? 'Yes' : '';
										}
								   }
								],
								additionalData: {
									quests: completedQuests
								}
								
							});
							$('#quests_pane').append(list.render());
							
							// render quests into element
							// make request to mark quests ad done/in progress
							
							
						},this
					);
				
				},this
			);
			
			
			
			
			
			
		
		},
		
	});
	
	return Quests;
})();


