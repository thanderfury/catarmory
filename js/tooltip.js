/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


 ;(function($) {
	var cache = new Array();
	var $el = null;

	function createElement() {
		if (!$el) {
			$el = $('<div style="position: absolute; width: 320px; min-height: 100px; background: black; top: 0; left: 0; display: none; color: #fff; border: 1px solid #ddd; border-radius: 5px; padding: 7px; font-family: Verdana, sans-serif; box-shadow: 0 0 1px 1px #000;"></div>');
			$('body').append($el);
		}
	};

	function showItem(e) {
		var entry = e.attr('data-entry');
		$el = $('<div style="width: 320px; min-height: 100px; background: black; color: #fff; border: 1px solid #ddd; border-radius: 5px; padding: 7px; font-family: Verdana, sans-serif; box-shadow: 0 0 1px 1px #000;"></div>');
		fetch({
			action: 'get_item',
			entry: entry
		},function(data) {
			$el.html(data.data.html);
			e.append($el);
		})
		
	};
	
	function show(e) {
		var guid = $(this).attr('data-guid');
		var entry = $(this).attr('data-entry');
		if (guid || entry) {
			fetch({
				action: 'get_item',
				guid: guid,
				entry: entry
			},function(data) {
				$el.html(data.data.html);
			})
		} else {
			$el.html('missing entry or guid');
		}
		$el.show();
		update(e);
	};

	function update(e) {
		var left = e.pageX+15;
		var top = e.pageY;
		if (top + $el[0].offsetHeight - $(window).scrollTop() + 20 >  $(window).height()) {
			top -= top + $el[0].offsetHeight - $(window).scrollTop() - $(window).height() + 20;
		}
		$el.css({
			left: left,
			top: top
		})
	}

	function hide() {
		$el.html('');
		$el.hide();
	};

	/*
	fetch - method for ajax jsonp request
	data: query parameters
	cb: callback evaluated on success
	_scope: scope in which callback will be evaluated
	*/
	function fetch(data,cb) {
		if (typeof(cache[data.guid+'_'+data.entry]) !== "undefined") {
			cb(cache[data.guid+'_'+data.entry]);
		} else {
			$.ajax({
				type: "GET",
				url: CONFIG.ajaxTooltipURL,
				data: data,
				dataType: "jsonp",
				error: function() {
					alert('error while requesting ajax gateway');
				},
				success: function(json) {
					cache[data.guid+'_'+data.entry] = json;
					cb(json);
				}
			});
		}
	};

	$.fn.extend({
		tooltip: function() {
			createElement();
			return this.each(function() {
			})
			.mouseover(show)
			.mouseout(hide)
			.mousemove(update);
		},
		showItem: function() {
			showItem(this);
		}
	});
	

	
})(jQuery);
