<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class Npcs extends Cache {

	protected $db;
	protected $dbh;

	/**
	 * Initialize new search
	 */
	function __construct($db) {
		$this->db = $db;
		$this->dbh = $db->dbh;
	}

	/**
	 * Search in npcs
	 * @return array npcs list
	 */
	public function search_by_name($name) {
		$get_npcs = $this->dbh->prepare('
			SELECT ct.`entry`,ct.`name`,ct.`subname`,ct.`minlevel`,ct.`maxlevel`
			FROM `'.$this->db->worlddb.'`.`creature_template` AS ct
			WHERE ct.`name` LIKE ? LIMIT '.SQL_LIMIT);
		$get_npcs->execute(array('%'.$name.'%'));	// consider adding fulltext over name filed in mysql and use AGAINST

		return $get_npcs->fetchAll(PDO::FETCH_ASSOC);
	}

}
