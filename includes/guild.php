<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Guild {

	protected $guild;
	protected $db;
	protected $dbh;

	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of arena team
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		if (intval($id) != 0) {
			$get_guild = $this->dbh->prepare('
				SELECT chg.`guildid`,chg.`name` AS guildName,chg.`level` AS guildLevel,chg.`experience`,chg.`todayExperience`,ch.`name` AS leaderName,ch.`race` AS leaderRace
				FROM `trinity_characters`.`guild` AS chg
				LEFT JOIN `trinity_characters`.`characters` AS ch ON (chg.`leaderguid`=ch.`guid`)
				WHERE chg.`guildid`=?');
			$get_guild->execute(array($id));
		} else if (is_string($id)) {
			$get_guild = $this->dbh->prepare('
				SELECT chg.`guildid`,chg.`name` AS guildName,chg.`level` AS guildLevel,chg.`experience`,chg.`todayExperience`,ch.`name` AS leaderName,ch.`race` AS leaderRace
				FROM `trinity_characters`.`guild` AS chg
				LEFT JOIN `trinity_characters`.`characters` AS ch ON (chg.`leaderguid`=ch.`guid`)
				WHERE chg.`name`=?');
			$get_guild->execute(array($id));
		}
		$this->guild = $get_guild->fetch(PDO::FETCH_ASSOC);

		$get_guild_members = $this->dbh->prepare('
			SELECT COUNT(*) AS num
			FROM `trinity_characters`.`guild_member` AS chgm
			WHERE chgm.guildid=?');
		$get_guild_members->execute(array($this->_get_guild_id()));
		$members = $get_guild_members->fetch(PDO::FETCH_ASSOC);
		$this->guild['members'] = $members['num'];
	}

	/**
	 * Returns guild guid
	 * @return integer guild guid
	 */
	private function _get_guild_id() {
		return $this->guild['guildid'];
	}
	
	/**
	 * Returns guild informations
	 * @return array guild informations
	 */
	public function get_guild() {
		if (!$this->guild['guildid'])
			return;

		return $this->guild;
	}
	
	// TODO:
	// guild member searching/matching
	
}
