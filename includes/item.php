<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Item extends Cache {

	protected $item;
	protected $db;
	protected $dbh;

	/**
	 * @param PDO database handler
	 * @param string|integer guid or name of arena team
	 */
	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;

		// search for cached data. Set variable and stop processing when found.
		if ($this->item = $this->get_cache(array('item',$id),ITEM_EXPIRE)) {
			return;
		}
		
		$get_item = $this->dbh->prepare('
			SELECT dis.`col_0` AS itemEntry,dis.`col_1` AS quality,dis.`col_12` AS itemLevel,dis.`col_9` AS inventoryType,dis.`col_99` AS name,LOWER(idi.`col_5`) AS icon
			FROM `db2_item_sparse` AS dis
			LEFT JOIN `db2_item` AS di ON (dis.`col_0`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE dis.`col_0` = ?');
		$get_item->execute(array($id));
		
		if ($get_item->rowCount() == 1) {
			$this->item = $get_item->fetch(PDO::FETCH_ASSOC);
			$this->store_cache(array('item',$id),$this->item);
		}
	}

	/**
	 * Returns item informations
	 * @return array item informations
	 */
	public function get_item() {
		if (!$this->item['itemEntry'])
			return;

		return $this->item;
	}

	/**
	 * Returns item name
	 * @return string item name
	 */
	public function get_name() {
		return $this->item['name'];
	}

	/**
	 * Returns item icon
	 * @return string item icon
	 */
	public function get_icon() {
		return $this->item['icon'];
	}
	
}
