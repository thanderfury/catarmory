<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class ItemSet {
	protected $db;
	protected $dbh;

	private $_itemset = array('id' => null, 'name' => null, 'items' => array(), 'bonuses' => array());

	function __construct($db,$guid) {
		$this->db = $db;
		$this->dbh = $db->dbh;
		$this->character_guid = $guid;
	}

	public function lookup_set($id) {
		$get_set = $this->dbh->prepare('SELECT `col_0` AS id,`col_1` AS name,`col_2` AS item_1,`col_3` AS item_2,`col_4` AS item_3,`col_5` AS item_4,`col_6` AS item_5,`col_7` AS item_6,`col_8` AS item_7,`col_9` AS item_8,`col_10` AS item_9,`col_11` AS item_10,`col_12` AS item_11,`col_13` AS item_12,`col_14` AS item_13,`col_15` AS item_14,`col_16` AS item_15,`col_17` AS item_16,`col_18` AS item_17,`col_19` AS spell_1,`col_20` AS spell_2,`col_21` AS spell_3,`col_22` AS spell_4,`col_23` AS spell_5,`col_24` AS spell_6,`col_25` AS spell_7,`col_26` AS spell_8,`col_27` AS count_1,`col_28` AS count_2,`col_29` AS count_3,`col_30` AS count_4,`col_31` AS count_5,`col_32` AS count_6,`col_33` AS count_7,`col_34` AS count_8,`col_35` AS skill,`col_36` AS value FROM `dbc_itemset` WHERE col_0=?');
		$get_set->execute(array($id));
		if ($get_set->rowCount() == 1) {
			$set = $get_set->fetch(PDO::FETCH_ASSOC);
			$this->_itemset['id'] = $set['id'];
			$this->_itemset['name'] = $set['name'];

			if ($this->character_guid) {
				$get_equipped_items = $this->dbh->prepare('
					SELECT COUNT(*) AS num
					FROM `trinity_characters`.character_inventory AS ci
					LEFT JOIN `trinity_characters`.item_instance AS ii ON (ci.`item`=ii.`guid`)
					LEFT JOIN `db2_item_sparse` AS dis ON (ii.`itemEntry`=dis.`col_0`)
					WHERE ci.`guid` = ? AND dis.`col_113` = ? AND ci.`bag`=0 AND ci.`slot` <= 19 GROUP BY dis.`col_113`');
				$get_equipped_items->execute(array($this->character_guid,$id));
				$equipped = $get_equipped_items->fetch(PDO::FETCH_ASSOC);
				$this->_itemset['equipped'] = $equipped['num'];

			}


			for ($i=1;$i<=17;++$i) {
				if ($set['item_'.$i] != 0) {
					$item = new Item($this->db,$set['item_'.$i]);
					$item->set_position = $i;
					$this->_itemset['items'][] = $item;
				}
			}

			for ($i=1;$i<=8;++$i) {
				if ($set['count_'.$i] != 0) {
					$spell = new Spell($this->db,$set['spell_'.$i]);
					$this->_itemset['bonuses'][] = array($set['count_'.$i],$spell);
				}
			}


		}

	}

	public function get_set_name() {
		return $this->_itemset['name'];
	}

	public function get_pieces() {
		return $this->_itemset['items'];
	}

	public function get_equipped_pieces() {
		return $this->_itemset['equipped'];
	}

	public function get_max_pieces() {
		return count($this->_itemset['items']);
	}

	public function get_set_bonuses() {
		$bonuses = $this->_itemset['bonuses'];
		usort($bonuses, array($this, "_usort_bonuses"));
		return $bonuses;
	}


	private function _usort_bonuses($a,$b) {
		return $a[0] > $b[0];
	}

}

/*
    //uint32    id                                          // 0        m_ID
    char* name;                                             // 1        m_name_lang
    uint32    itemId[MAX_ITEM_SET_ITEMS];                   // 2-18     m_itemID
    uint32    spells[MAX_ITEM_SET_SPELLS];                  // 19-26    m_setSpellID
    uint32    items_to_triggerspell[MAX_ITEM_SET_SPELLS];   // 27-34    m_setThreshold
    uint32    required_skill_id;                            // 35       m_requiredSkill
    uint32    required_skill_value;                         // 36       m_requiredSk

*/