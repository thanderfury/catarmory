<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

$shared = array();

$shared['ItemBondingType'] = array(
	'',				// 0
	'Binds when picked up',		// 1
	'Binds when equipped',	// 2
	'Binds when used',		// 3
	'Quest Item',			// 4
);

$shared['InventoryType'] = array(
	'',			// 0
	'Head',			// 1
	'Neck',			// 2
	'Shoulders',		// 3
	'Body',			// 4
	'Chest',		// 5
	'Waist',		// 6
	'Legs',			// 7
	'Feet',			// 8
	'Wrist',		// 9
	'Hands',		// 10
	'Finger',		// 11
	'Trinket',		// 12
	'Weapon',		// 13
	'Shield',		// 14
	'Ranged',		// 15
	'Cloak',		// 16
	'Two-Hand',		// 17
	'Bag',			// 18
	'Tabard',		// 19
	'Robe',			// 20
	'Main-Hand',		// 21
	'Off-Hand',		// 22
	'Holdable',		// 23
	'Ammo',			// 24
	'Thrown',		// 25
	'Right-Hand Ranged',	// 26
	'Quiver',		// 27
	'Relic'			// 28
);

$shared['ItemClass'] = array(
	array( // consumable | 0,
		'Consumable', 		// 0
		'Potion',		// 1
		'Elixir',		// 2
		'Flask',		// 3
		'Scroll',		// 4
		'Food/Drink',		// 5
		'Item Enhancement',	// 6
		'Bandage',		// 7
		'Other Consumable'	// 8
	),
	array( // container | 1
		'Container',		// 0
		'Soul Bag',		// 1
		'Herbalism Bag',	// 2
		'Enchanting Bag',	// 3
		'Engineering Bag',	// 4
		'Gem Bag',		// 5
		'Mining Bag',		// 6
		'Leatherworking Bag',	// 7
		'Inscription Bag',	// 8
		'Tackle Bag',		// 9
	),
	array( // weapon | 2
		'Axe',			// 0
		'Axe',			// 1
		'Bow',			// 2
		'Gun',			// 3
		'Mace',			// 4
		'Mace',			// 5
		'Polearm',		// 6
		'Sword',		// 7
		'Sword',		// 8
		'',			// 9
		'Staff',		// 10
		'Exotic',		// 11
		'Exotic',		// 12
		'Fist',			// 13
		'Miscellaneous',	// 14
		'Dagger',		// 15
		'Thrown',		// 16
		'Spear',		// 17
		'Crossbow',		// 18
		'Wand',			// 19
		'Fishing Pole',		// 20
	),
	array( // gem | 3
		'Red',			// 0
		'Blue',			// 1
		'Yellow',		// 2
		'Purple',		// 3
		'Green',		// 4
		'Orange',		// 5
		'Meta',			// 6
		'Simple',		// 7
		'Prismatic',		// 8
		'Hydraulic',		// 9
		'Cogwheel',		// 10
	),
	array( // armor | 4
		'Miscellaneous',	// 0
		'Cloth',		// 1
		'Leather',		// 2
		'Mail', 		// 3
		'Plate', 		// 4
		'Buckler',		// 5
		'Shield',		// 6
		'Libram',		// 7
		'Idol', 		// 8
		'Totem',	 	// 9
		'Sigil',	 	// 10
		'Relic',	 	// 11
	),
	array( // reagent | 5
		'Reagent',		// 0

	),
	array( // projectile | 6
		'',			// 0
		'',			// 1
		'Arrow',		// 2
		'Bullet',		// 3
		'',			// 4
	),
/*	array( // 
		'', // 

	),
	array( // 
		'', // 

	),


    ITEM_CLASS_TRADE_GOODS                      = 7,
    ITEM_CLASS_GENERIC                          = 8,  // OBSOLETE
    ITEM_CLASS_RECIPE                           = 9,
    ITEM_CLASS_MONEY                            = 10, // OBSOLETE
    ITEM_CLASS_QUIVER                           = 11,
    ITEM_CLASS_QUEST                            = 12,
    ITEM_CLASS_KEY                              = 13,
    ITEM_CLASS_PERMANENT                        = 14, // OBSOLETE
    ITEM_CLASS_MISCELLANEOUS                    = 15,
    ITEM_CLASS_GLYPH                            = 16
*/
);


$shared['ItemModType'] = array(
	'Mana',				// 0 
	'Health',			// 1
	'',				// 2
	'Agility',			// 3
	'Strength',			// 4
	'Intelect',			// 5
	'Spirit',			// 6
	'Stamina',			// 7
	'',				// 8
	'',				// 9
	'',				// 10
	'',				// 11
	'Defense Rating',		// 12
	'Dodge Rating',			// 13
	'Parry Rating',			// 14
	'Block Rating',			// 15
	'Melee Hit Rating',		// 16
	'Ranged Hit Rating',		// 17
	'Spell Hit Rating',		// 18
	'Melee Crit Rating',		// 19
	'Ranged Crit Rating',		// 20
	'Spell Crit Rating',		// 21
	'HIT_TAKEN_MELEE_RATING',	// 22
	'HIT_TAKEN_RANGED_RATING',	// 23
	'HIT_TAKEN_SPELL_RATING',	// 24
	'CRIT_TAKEN_MELEE_RATING',	// 25
	'CRIT_TAKEN_RANGED_RATING',	// 26
	'CRIT_TAKEN_SPELL_RATING',	// 27
	'Melee Haste Rating',		// 28
	'Ranged Haste Rating',		// 29
	'Spell Haste Rating',		// 30
	'Hit Rating',			// 31
	'Crit Rating',			// 32
	'Hit Taken Rating',		// 33
	'Crit Taken Rating',		// 34
	'Resilience Rating',		// 35
	'Haste Rating',			// 36
	'Expertise Rating',		// 37
	'Attack Power',			// 38
	'Ranged Attack Power',		// 39
	'',				// 40
	'',				// 41
	'',				// 42
	'Mana Regeneration',		// 43
	'Armor Penetration Rating',	// 44
	'Spell Power',			// 45
	'Health Regeneration',		// 46
	'Spell Penetration',		// 47
	'Block Value',			// 48
	'Mastery Rating',		// 49
	'Extra Armor',			// 50
	'Fire Resistance',		// 51
	'Frost Resistance',		// 52
	'Holy Resistance',		// 53
	'Shadow Resistance',		// 54
	'Nature Resistance',		// 55
	'Arcane Resistance' 		// 56
);


$shared['SocketColor'] = array(
	'1' => 'meta',
	'2' => 'red',
	'4' => 'yellow',
	'6' => 'orange',
	'8' => 'blue',
	'10' => 'purple',
	'12' => 'green',
	'14' => 'prismatic',
	'16' => 'hydraulic',
	'32' => 'cogwheel'
);

$shared['Classes'] = array(
	'',		// 0
	'Warrior',	// 1
	'Paladin',	// 2
	'Hunter',	// 3
	'Rogue',	// 4
	'Priest',	// 5
	'Death Knight',	// 6
	'Shaman',	// 7
	'Mage',		// 8
	'Warlock',	// 9
	'',		// 10
	'Druid',	// 11
);

$shared['Races'] = array(
	'',		// 0
	'Human',	// 1
	'Orc',		// 2
	'Dwarf',	// 3
	'Night Elf',	// 4
	'Undead',	// 5
	'Tauren',	// 6
	'Gnome',	// 7
	'Troll',	// 8
	'Goblin',	// 9
	'Blood Elf',	// 10
	'Draenei',	// 11
	'',		// 12
	'',		// 13
	'',		// 14
	'',		// 15
	'',		// 16
	'',		// 17
	'',		// 18
	'',		// 19
	'',		// 20
	'',		// 21
	'Worgen',	// 22
	'',		// 23
);



$shared['ItemColors'] = array(
	'#9d9d9d', 	// 0 poor - grey
	'#fff',		// 1 common - white
	'#1eff00',	// 2 uncommon - green
	'#0070ff',	// 3 rare - blue
	'#a335ee',	// 4 epic - purple
	'#ff8000',	// 5 legendary - orange
	'#e6cc80',	// 6 artifact - yellow
	'#e6cc80',	// 7 aheirloom - yellow
);

class TooltipData extends Cache {

	protected $db;
	protected $dbh;

	function __construct($db) {
		$this->db = $db;
		$this->dbh = $db->dbh;
	}


	public function get_item($guid=0,$entry=0) {
		global $shared;

		if ($out = $this->get_cache(array('tooltip',($guid ? '_guid_'.$guid : '_entry_'.$entry)),($guid ? TOOLTIP_EXPIRE : ITEM_EXPIRE))) {
			return $out;
		}

		if ($guid != 0) {
			$get_item = $this->dbh->prepare('
			SELECT
ii.`itemEntry`,ii.`count`,ii.`enchantments`,ii.`owner_guid`,
dis.`col_1` AS quality,dis.`col_8` AS SellPrice,dis.`col_9` AS inventoryType,dis.`col_10` AS allowableClass,dis.`col_11` AS allowableRace,dis.`col_12` AS itemLevel,dis.`col_13` AS requiredLevel,dis.`col_14` AS requiredSkill,dis.`col_15` AS requiredSkillRank,dis.`col_16` AS requiredSpell,dis.`col_17` AS requiredHonorRank,dis.`col_18` AS requiredCityRank,dis.`col_19` AS requiredReputationFaction,dis.`col_20` AS requiredReputationRank,dis.`col_23` AS ContainerSlots,dis.`col_24` AS statType_0,dis.`col_25` AS statType_1,dis.`col_26` AS statType_2,dis.`col_27` AS statType_3,dis.`col_28` AS statType_4,dis.`col_29` AS statType_5,dis.`col_30` AS statType_6,dis.`col_31` AS statType_7,dis.`col_32` AS statType_8,dis.`col_33` AS statType_9,dis.`col_34` AS statValue_0,dis.`col_35` AS statValue_1,dis.`col_36` AS statValue_2,dis.`col_37` AS statValue_3,dis.`col_38` AS statValue_4,dis.`col_39` AS statValue_5,dis.`col_40` AS statValue_6,dis.`col_41` AS statValue_7,dis.`col_42` AS statValue_8,dis.`col_43` AS statValue_9,dis.`col_98` AS bonding,dis.`col_99` AS name,dis.`col_113` AS item_set,dis.`col_118` AS socket_1,dis.`col_119` AS socket_2,dis.`col_120` AS socket_3,dis.`col_124` AS socket_bonus,
LOWER(idi.`col_5`) AS icon,
di.`col_1` AS itemClass,di.`col_2` AS itemSubClass
			FROM `'.$this->db->characterdb.'`.`item_instance` AS ii
			LEFT JOIN `db2_item_sparse` AS dis ON (ii.`itemEntry`=dis.`col_0`)
			LEFT JOIN `db2_item` AS di ON (ii.`itemEntry`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE ii.`guid`=?');
			$get_item->execute(array($guid));

		} else if ($entry != 0) {
			$get_item = $this->dbh->prepare('
			SELECT
"1" AS `count`,"" AS `enchantments`,
dis.`col_0` AS `itemEntry`,dis.`col_1` AS quality,dis.`col_8` AS SellPrice,dis.`col_9` AS inventoryType,dis.`col_10` AS allowableClass,dis.`col_11` AS allowableRace,dis.`col_12` AS itemLevel,dis.`col_13` AS requiredLevel,dis.`col_14` AS requiredSkill,dis.`col_15` AS requiredSkillRank,dis.`col_16` AS requiredSpell,dis.`col_17` AS requiredHonorRank,dis.`col_18` AS requiredCityRank,dis.`col_19` AS requiredReputationFaction,dis.`col_20` AS requiredReputationRank,dis.`col_23` AS ContainerSlots,dis.`col_24` AS statType_0,dis.`col_25` AS statType_1,dis.`col_26` AS statType_2,dis.`col_27` AS statType_3,dis.`col_28` AS statType_4,dis.`col_29` AS statType_5,dis.`col_30` AS statType_6,dis.`col_31` AS statType_7,dis.`col_32` AS statType_8,dis.`col_33` AS statType_9,dis.`col_34` AS statValue_0,dis.`col_35` AS statValue_1,dis.`col_36` AS statValue_2,dis.`col_37` AS statValue_3,dis.`col_38` AS statValue_4,dis.`col_39` AS statValue_5,dis.`col_40` AS statValue_6,dis.`col_41` AS statValue_7,dis.`col_42` AS statValue_8,dis.`col_43` AS statValue_9,dis.`col_98` AS bonding,dis.`col_99` AS name,dis.`col_113` AS item_set,dis.`col_118` AS socket_1,dis.`col_119` AS socket_2,dis.`col_120` AS socket_3,dis.`col_124` AS socket_bonus,
LOWER(idi.`col_5`) AS icon,
di.`col_1` AS itemClass,di.`col_2` AS itemSubClass
			FROM `db2_item_sparse` AS dis
			LEFT JOIN `db2_item` AS di ON (dis.`col_0`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE dis.`col_0`=?');
			$get_item->execute(array($entry));
		} else {
			return false;
		}

		$get_item->execute(array($guid ? $guid : $entry));

		if ($get_item->rowCount() == 1) {

			$out = array();
			$o = $get_item->fetch(PDO::FETCH_ASSOC);

			$item_enchants = new ItemEnchants($this->db);
			$item_enchants->parse($o['enchantments']);



			$html = '<div style="line-height: 17px; font-size: 12px; color: #fff;">';
			$html .= '<div style="font-size: 14px; line-height: 19px; color:'.$shared['ItemColors'][$o['quality']].'">'.$o['name'].'</div>';
			$html .= '<div style="color: #ffd100">Item Level '.$o['itemLevel'].'</div>';
			$html .= '<div>'.$shared['ItemBondingType'][$o['bonding']].'</div>';
			if ($o['itemClass'] == 1) { // bag
				$html .= '<div>'.$o['ContainerSlots'].' Slot '.$shared['InventoryType'][$o['inventoryType']].'</div>';
			} else { // dont show inventory type and subclass on certain items (like bags)
				$html .= '<div style="float: left">'.$shared['InventoryType'][$o['inventoryType']].'</div>';
				$html .= '<div style="float: right">'.$shared['ItemClass'][$o['itemClass']][$o['itemSubClass']].'</div>';
				$html .= '<div style="clear: both"></div>';
			}
			if ($o['itemClass'] == 4) {
				// armor calculation - 7.14 looks like contant for legs. Need to rework!!!
				$html .= '<div>';
				if ($o['itemSubClass'] >=1 && $o['itemSubClass'] <=4) {
					$get_armor = $this->dbh->prepare('SELECT `col_2` AS cloth,`col_3` AS leather,`col_4` AS mail,`col_5` AS plate FROM `dbc_itemarmortotal` WHERE `col_1`=?');
					$get_armor->execute(array($o['itemLevel']));
					$a = $get_armor->fetch(PDO::FETCH_ASSOC);
					switch ($o['itemSubClass']) {
						case '1': $html .= intval($a['cloth'] / 7.14); break;
						case '2': $html .= intval($a['leather'] / 7.14); break;
						case '3': $html .= intval($a['mail'] / 7.14); break;
						case '4': $html .= intval($a['plate'] / 7.14); break;
					}
				}
				$html .= ' Armor</div>';
			}

			for ($i=0;$i<=9;++$i) {		// base stat (white)
				if ($o['statType_'.$i] != -1 && $o['statType_'.$i] > 0 && ($o['statType_'.$i] >= 3 && $o['statType_'.$i] <=7)) {
					$html .= '<div>+'.$o['statValue_'.$i].' '.$shared['ItemModType'][$o['statType_'.$i]].'</div>';
				}
			}


			$e = $item_enchants->get_item_enchant(0);	// permanent enchantments
			if ($e[0]) {
				$html .= '<div style="color: #1eff00">'.$e[3].'</div>';
			}

			for ($i=0;$i<=9;++$i) {		// green stat (on equip)
				if ($o['statType_'.$i] != -1 && $o['statType_'.$i] > 0 && ($o['statType_'.$i] < 3 || $o['statType_'.$i] > 7)) {
					$html .= '<div style="color:#1eff00">Equip: +'.$o['statValue_'.$i].' '.$shared['ItemModType'][$o['statType_'.$i]].'</div>';
				}
			}


			// gem sockets
			if ($o['socket_bonus']) {
				$html .= '<br />';
				for ($i=1;$i<=3;++$i) {
					if ($o['socket_'.$i] != 0) {
						$e = $item_enchants->get_item_enchant($i+1);
						$gem = new SpellItemEnchantment($this->db,$e[0]);
						$html .= '<div><img src="http://new.wowarmory.sk/images/socket-'.$shared['SocketColor'][$o['socket_'.$i]].'.gif"> ';
						$color = $gem->get_gem_color();
						if ($color) {
							$html .= ' <img src="http://new.wowarmory.sk/images/inv_jewelcrafting_'.$color.'.png"> ';
						}
						$html .= ($e[0] != 0 ? $e[3] : $shared['SocketColor'][$o['socket_'.$i]].' socket') .'</div>';
					}
				}

				$e = $item_enchants->get_item_enchant(6);	// buckle (PRISMATIC_ENCHANTMENT_SLOT)
				if ($e[0]) {
					$e2 = $item_enchants->get_item_enchant(4);
					$gem = new SpellItemEnchantment($this->db,$e2[0]);
					$html .= '<div style="color:#1eff00"><img src="http://new.wowarmory.sk/images/socket-prismatic.gif"> ';
					$color = $gem->get_gem_color();
					if ($color) {
						$html .= ' <img src="http://new.wowarmory.sk/images/inv_jewelcrafting_'.$color.'.png"> ';
					}
					$html .= $e2[3].'</div>';
				}


				$e = $item_enchants->get_item_enchant(5);
				$socket_bonus = new SpellItemEnchantment($this->db,$o['socket_bonus']);
				$html .= '<div style="color: '.($e[0] != 0 ? '#1eff00' : '#9d9d9d' ).'">Socket bonus:'.$socket_bonus->get_name().'</div>';
			}

/*
    PROP_ENCHANTMENT_SLOT_0         = 10,                   // used with RandomSuffix
    PROP_ENCHANTMENT_SLOT_1         = 11,                   // used with RandomSuffix
    PROP_ENCHANTMENT_SLOT_2         = 12,                   // used with RandomSuffix and RandomProperty
    PROP_ENCHANTMENT_SLOT_3         = 13,                   // used with RandomProperty
    PROP_ENCHANTMENT_SLOT_4         = 14,                   // used with RandomProperty
    MAX_ENCHANTMENT_SLOT            = 15
*/


			$html .= '<br />';
			if ($o['itemClass'] == 2 || $o['itemClass'] == 4) {
				if ($guid != 0) {
					$html .= '<div>Durability ?/?</div>';	// game/Globals/ObjectMgr.cpp :: FillMaxDurability()
				} else if ($entry != 0) {
					$html .= '<div>Durability ?</div>';
				}
			}

			if ($o['allowableClass'] > 0 && $o['allowableClass'] != 262143 && $o['allowableClass'] != 1535 ) {
				$classes = array();
				for ($i=1;$i<sizeof($shared['Classes']);++$i) {
					if ($o['allowableClass'] & (1<<($i-1))) {
						$classes[] = $shared['Classes'][$i];
					}
				}
				$html .= '<div>Classes: '.implode(', ',$classes).'</div>';
			}
			if ($o['allowableRace'] > 0 && $o['allowableRace'] != 2147483647) {
				$races = array();
				for ($i=1;$i<sizeof($shared['Races']);++$i) {
					if ($o['allowableRace'] & (1<<($i-1))) {
						$races[] = $shared['Races'][$i];
					}
				}
				$html .= '<div>Races: '.implode(', ',$races).'</div>';
			}
			if ($o['requiredLevel'] > 0) {
				$html .= '<div>Requires Level '.$o['requiredLevel'].'</div>';
			}




/*
    int32      AllowableClass;                               // 10      7
    int32      AllowableRace;                                // 11      8
    int32      RequiredLevel;                                // 13      10
    uint32     RequiredSkill;                                // 14      11
    uint32     RequiredSkillRank;                            // 15      12
    uint32     RequiredSpell;                                // 16      13
    uint32     RequiredHonorRank;                            // 17      14
    uint32     RequiredCityRank;                             // 18      15
    uint32     RequiredReputationFaction;                    // 19      16
    uint32     RequiredReputationRank;                       // 20      17
*/


			if ($o['item_set'] > 0) {
				$html .= '<br />';
				$item_set = new ItemSet($this->db,$o['owner_guid']);
				$item_set->lookup_set($o['item_set']);
				$html .= '<div style="color:#ffd100">'.$item_set->get_set_name().' ('.$item_set->get_equipped_pieces().'/'.$item_set->get_max_pieces().')</div>';
				foreach ($item_set->get_pieces() as $p) {
					$html .= '<div style="color: #9d9d9d; padding-left: 9px;">'.$p->get_name().'</div>';
				}

				$html .= '<br />';

				foreach ($item_set->get_set_bonuses() as $b) {
					$html .= '<div style="color: '. ($item_set->get_equipped_pieces() >= $b[0] ? '#1eff00' : '#9d9d9d').'">('.$b[0].') Set: '.$b[1]->get_name().'</div>';

				}
			}

			if ($o['SellPrice'] > 0) {
				$html .= '<div>Sell Price: '.$this->_calculate_money($o['SellPrice']).'</div>';
			}
			
			
			$html .= '<div style="padding-top: 5px; font-size: 10px; color: #9d9d9d; float: right">from CatArmory</div>';
                        $html .= '<div style="clear: both"></div>';

			$html .= '</div>';

			$out = array(
				'icon' => $o['icon'],
				'html' => $html
			);
		} else {
			$out = array(
				'icon' => 'unknown',
				'html' => 'item not found'
			);
		}
		$this->store_cache(array('tooltip',($guid ? '_guid_'.$guid : '_entry_'.$entry)),$out);

		return $out;
	}

	/**
	 * Search in characters
	 * @param integer int representation of money
	 * @return string html representation of money
	 */
	private function _calculate_money($int) {
		$gold = intval($int / 10000);
		$tmp = $int % 10000;
		$silver = intval($tmp / 100);
		$copper = $tmp % 100;
		return ($gold ? $gold.'<img src="http://new.wowarmory.sk/images/g.gif"> ' : '').(($silver || $int < 100) ? $silver.'<img src="http://new.wowarmory.sk/images/s.gif"> ' : '').($copper ? $copper.'<img src="http://new.wowarmory.sk/images/c.gif">' : '');
	}
	
}

