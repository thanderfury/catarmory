<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Spell {

	protected $db;
	protected $dbh;

	private $_spell;

	function __construct($db,$id) {
		$this->db = $db;
		$this->dbh = $db->dbh;
		$get_spell = $this->dbh->prepare('SELECT `col_23` AS name FROM `dbc_spell` WHERE `col_0`=?');
		$get_spell->execute(array($id));
		$this->_spell = $get_spell->fetch(PDO::FETCH_ASSOC);
	}

	public function get_name() {
		return $this->_spell['name'];
	}

}

